#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<time.h>
#include "sqlite3.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void soruEkle();
void soruGetir();
void soruGuncelle();
void soruSil();

void soruCoz();
void soruSayisi();

char cevabim;
int sonuc;
int sorulistesi[];
int sorusayisi = 0;

int pulling = 0;


sqlite3 *db;
char *zErrMsg = 0;
int rc;
char *sql;
const char* data = "Callback function called";
int katilimciId = 0;
int katilimciVar = 0;

char statement[300];

unsigned long ToUInt(char* str)
{
    unsigned long mult = 1;
    unsigned long re = 0;
    int len = strlen(str);
    int i;
    for(i = len -1 ; i >= 0 ; i--)
    {
        re = re + ((int)str[i] -48)*mult;
        mult = mult*10;
    }
    return re;
}

static int freecallback(void *NotUsed, int argc, char **argv, char **azColName) {
	return 0;
}
static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}
static int iskatilimci(void *NotUsed, int argc, char **argv, char **azColName) {
	katilimciVar = katilimciId;
	return 0;
}
static int soruSayisiCB(void *NotUsed, int argc, char **argv, char **azColName) {
	sorusayisi = ToUInt(argv[0]);
	return 0;
}
static int soruIdListesi(void *NotUsed, int argc, char **argv, char **azColName) {
	if(pulling == 0) {
		sorusayisi = 1;
		pulling = 1;
	}else{
		sorusayisi++;
	}
	sorulistesi[sorusayisi-1] = ToUInt(argv[0]);
	return 0;
}
static int soruCiktisi(void *NotUsed, int argc, char **argv, char **azColName) {
	if(argc > 0) {
		printf(">> %s\n", azColName[1]);
		int durum = rand()%2;
		if(durum) {
			printf(">>A) %s\n", argv[2]);
			printf(">>B) %s\n", argv[3]);
		}else{
			printf(">>A) %s\n", argv[3]);
			printf(">>B) %s\n", argv[2]);
		}
		printf("\n");
		scanf("%c", &cevabim);
		getchar();
		if(cevabim == 'a' || cevabim == 'A') {
			if(durum == 1) sonuc = 1;
			else sonuc = 0;
		}else if(cevabim == 'b' || cevabim == 'B'){
			if(durum == 0) sonuc = 1;
			else sonuc = 0;
		}else{
			sonuc = 0;
		}
	}
}

int main() {
	
	int rol = 0;
	srand(time(NULL));
	
	rc = sqlite3_open("data.db", &db);
	
	if(rc){
		printf("Veritabanina baglanilamadi!");
		return 0;
	}
	
	printf("\n===== Bir Dogru Bir Cevap ====\n\n");
	printf("\nAdmin olarak devam etmek icin (1), Katilimci olarak devam etmek icin (0) giriniz: ");
	scanf("%d", &rol);
	getchar();
	
	if(rol == 1){
		int nextstep = 1;
		int kod = 0;
		while (nextstep)
		{
			printf("\nYapmak istediginiz islemin kodunu giriniz, mesela yardim icin (0): ");
			scanf("%d", &kod);
			getchar();
			switch (kod)
			{
			case -1:
				nextstep = 0;
				break;
			case 0:
				printf("\n(-1) Oturumu kapat\n(0) Yardim\n(1) Yeni soru ekle\n(2) Soruyu goruntule\n(3) Soru guncelle\n(4) Soru sil\n(5) En zor N soruyu goster\n(6) Katilimci sayisini getir\n(7) Kac katilimci en az N soru cozmus\n(8) Excel formatinda yuzeysel documan al\n(10) Excel formatinda ayrintili documan al");
				break;
			case 1:
				soruEkle();
				break;
			case 2:
				soruGetir();
				break;
			case 3:
				soruGuncelle();
				break;
			case 4:
				soruSil();
				break;
			default:
				nextstep = 0;
				break;
			}
		}
	}else{
		int nextstep = 1;
		int kod = 0;
		
		printf("\nKatilimci numaranizi giriniz: ");
		scanf("%d", &katilimciId);
		getchar();
		sprintf(statement, " SELECT * FROM katilimci WHERE id=%d", katilimciId);
		sql = statement;
		rc = sqlite3_exec(db, sql, iskatilimci, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		if (katilimciVar == 0) {
			sprintf(statement, " INSERT INTO katilimci (id) VALUES (%d)", katilimciId);
			sql = statement;
			rc = sqlite3_exec(db, sql, freecallback, (void*)data, &zErrMsg);
		}
		
		while (nextstep)
		{
			printf("\nYapmak istediginiz islemin kodunu giriniz, mesela yardim icin (0): ");
			scanf("%d", &kod);
			getchar();
			switch (kod)
			{
			case -1:
				nextstep = 0;
				break;
			case 0:
				printf("\n(-1) Oturumu kapat\n(0) Yardim\n(1) Soru coz\n(2) Guncel istatistigim\n(3) Basari siralamam\n(4) Excel'e aktar\n");
				break;
			case 1:
				soruCoz();
				break;
			case 2:
				soruSayisi();
				break;
			case 3:
				
				break;
			case 4:
				
				break;
				
			default:
				nextstep = 0;
				break;
			}
		}
	}
	
	sqlite3_close(db);
	//system("pause");
	return 1;
}

void soruEkle(){
	char soruText[128], cevapDText[128], cevapYText[128];

	printf("\nSoruyu yaziniz: ");
	scanf("%[^\n]s", soruText);
	getchar();
	printf("\nDogru cevabi yaziniz: ");
	scanf("%[^\n]s", cevapDText);
	getchar();
	printf("\nYanlis cevabi yaziniz: ");
	scanf("%[^\n]s", cevapYText);
	sprintf(statement, " INSERT INTO soru (soruText, cevapDText, cevapYText) VALUES ('%s','%s','%s')", soruText, cevapDText, cevapYText);
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
}

void soruGetir(){
	int id;
	char soruText[128], cevapDText[128], cevapYText[128];
	printf("\nGetirmek istediginiz sorunun id'sini giriniz\nVeya butun sorularin id'sini listelemek icin (0):\n");
	scanf("%d", &id);
	if(id == 0) {
		sql = "SELECT id FROM soru";
		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}else{
			fprintf(stdout, "\n");
		}
	}else{
		sprintf(statement, " SELECT * FROM soru WHERE id=%d", id);
		sql = statement;
		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}else{
			fprintf(stdout, "\n");
		}
		
	}
}

void soruGuncelle(){
	int id;
	char soruText[128], cevapDText[128], cevapYText[128];
	printf("\nGuncellemek istediginiz sorunun id'sini giriniz: ");
	scanf("%d", &id);
	getchar();
	sprintf(statement, " SELECT * FROM soru WHERE id=%d", id);
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}else{
		fprintf(stdout, "\n");
	}
	
	printf("\n\nYeni Soruyu yaziniz: ");
	scanf("%[^\n]s", soruText);
	getchar();
	printf("\nYeni Dogru cevabi yaziniz: ");
	scanf("%[^\n]s", cevapDText);
	getchar();
	printf("\nYeni Yanlis cevabi yaziniz: ");
	scanf("%[^\n]s", cevapYText);
	sprintf(statement, " UPDATE soru SET soruText = '%s', cevapDText = '%s', cevapYText = '%s' WHERE id = %d", soruText, cevapDText, cevapYText, id);
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
}
void soruSil(){
	
	int id;
	printf("\nSilmek istediginiz sorunun id'sini giriniz\n");
	scanf("%d", &id);
	sprintf(statement, " DELETE from soru where id=%d", id);
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if( rc != SQLITE_OK ) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
}
void soruCoz(){
	int id;
	char cevap;
	
	sql = "SELECT id FROM soru";
	rc = sqlite3_exec(db, sql, soruIdListesi, (void*)data, &zErrMsg);
	pulling = 0;
	id = sorulistesi[rand()%sorusayisi];
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	
	sprintf(statement, " SELECT * FROM soru WHERE id=%d", id);
	sql = statement;
	rc = sqlite3_exec(db, sql, soruCiktisi, (void*)data, &zErrMsg);
	
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	
	
	
	if(sonuc == 1) {
		sprintf(statement, " UPDATE soru SET goruldu = goruldu + 1, dogru = dogru + 1 WHERE id=%d", id);
	}else{
		sprintf(statement, " UPDATE soru SET goruldu = goruldu + 1, yanlis = yanlis + 1 WHERE id=%d", id);
	}
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	
	if(sonuc == 1) {
		sprintf(statement, " UPDATE katilimci SET katildi = katildi + 1, dogru = dogru + 1 WHERE id=%d", katilimciId);
	}else{
		sprintf(statement, " UPDATE katilimci SET katildi = katildi + 1, yanlis = yanlis + 1 WHERE id=%d", katilimciId);
	}
	sql = statement;
	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	
}
void soruSayisi() {
	//sorusayisi
		sql = "SELECT count() FROM soru";
	rc = sqlite3_exec(db, sql, soruSayisiCB, (void*)data, &zErrMsg);
	
	if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL hatasi: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		printf("Soru sayisi: %d\n", sorusayisi);
}


